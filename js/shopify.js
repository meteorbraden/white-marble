import Client from 'shopify-buy';

const client = Client.buildClient({
  domain: 'https://white-marble-company.myshopify.com/',
  storefrontAccessToken: '5c400b6cd3e376e3b58ca2937b0c7aeb'
});

// Fetch all products in your shop
client.product.fetchAll().then((products) => {
  // Do something with the products
  console.log(products);
});

// Fetch a single product by ID
const productId = 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc4NTc5ODkzODQ=';

client.product.fetch(productId).then((product) => {
  // Do something with the product
  console.log(product);
});